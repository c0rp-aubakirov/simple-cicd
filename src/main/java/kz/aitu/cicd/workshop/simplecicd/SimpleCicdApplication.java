package kz.aitu.cicd.workshop.simplecicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleCicdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleCicdApplication.class, args);
	}

}
