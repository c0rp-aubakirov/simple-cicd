FROM softrackkz/alpine-jdk:8-252

MAINTAINER Sanzhar Aubakirov, saubakirov@innoforce.kz

WORKDIR /
ARG CI_PROJECT_NAME
ADD target/${CI_PROJECT_NAME}.jar app.jar

CMD [ "sh", "-c", "java $JVM_ARGS -jar /app.jar $JAVA_APP_ARGS" ]
